import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Notfound from '@/components/Notfound'
import Contact from '@/components/contact/Contact'
import AddContact from '@/components/contact/AddContact'
import EditContact from '@/components/contact/EditContact'
import Gallery from '@/components/gallery/Gallery'
import AddGallery from '@/components/gallery/AddGallery'
import Resident from '@/components/resident/Resident'
import AddResident from '@/components/resident/AddResident'
import EditResident from '@/components/resident/EditResident'
import Activity from '@/components/activities/Activity'
import AddActivity from '@/components/activities/AddActivity'
import EditActivity from '@/components/activities/EditActivity'
import News from '@/components/news/News'
import AddNews from '@/components/news/AddNews'
import EditNews from '@/components/news/EditNews'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/addContact',
      name: 'AddContact',
      component: AddContact
    },
    {
      path: '/editContact',
      name: 'EditContact',
      component: EditContact
    },
    {
      path: '/gallery',
      name: 'Gallery',
      component: Gallery
    },
    {
      path: '/addGallery',
      name: 'AddGallery',
      component: AddGallery
    },
    {
      path: '/resident',
      name: 'Resident',
      component: Resident
    },
    {
      path: '/addResident',
      name: 'AddResident',
      component: AddResident
    },
    {
      path: '/editResident',
      name: 'EditResident',
      component: EditResident
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity
    },
    {
      path: '/addActivity',
      name: 'AddActivity',
      component: AddActivity
    },
    {
      path: '/editActivity',
      name: 'EditActivity',
      component: EditActivity
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/addNews',
      name: 'AddNews',
      component: AddNews
    },
    {
      path: '/editNews',
      name: 'EditNews',
      component: EditNews
    },
  ]
})
